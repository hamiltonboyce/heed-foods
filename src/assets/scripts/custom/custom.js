import $ from 'jquery';
import Variants from '@shopify/theme-variants';

// Accordions

$( '.js-accordion-heading' ).click( function() {
  $( this ).next( '.js-accordion-content' ).slideToggle();
});