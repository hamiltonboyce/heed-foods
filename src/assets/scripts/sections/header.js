import $ from 'jquery';

$( '.js-announcement-close' ).click( function() {
	$( '.announcement-bar' ).slideUp();
	// add cookie functionality - https://alexcican.com/post/set-cookies-javascript/
});

$( '.js-menu-open' ).click( function() {
	$( 'body' ).addClass( 'menu-is-open' );
});

$( '.js-menu-close' ).click( function() {
	$( 'body' ).removeClass( 'menu-is-open' );
});

// $( '.has-sub-nav.page-nav-item--mobile' ).one( "click", function(event) {
// 	$( this ).find( '.sub-nav-list' ).slideToggle();
// 	event.preventDefault();
// });

$( '.js-header' ).clone( true ).insertAfter( $( '.js-header' ) ).addClass( 'floating-header' ).removeClass( 'js-header' );

import Headroom from 'headroom.js';

// grab an element
var header = document.querySelector( '.floating-header' );

// options
var options = {
    offset : 200,
    tolerance : {
        down : 0,
        up : 10
    }
};

if ( header ) {

	// construct an instance of Headroom, passing the element
	var headroom  = new Headroom( header, options );

	// initialize
	headroom.init( );

}