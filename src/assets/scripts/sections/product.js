/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */

import $ from 'jquery';
import Variants from '@shopify/theme-variants';
import {formatMoney} from '@shopify/theme-currency';
import sections from '@shopify/theme-sections';

const selectors = {
  addToCart: '[data-add-to-cart]',
  addToCartText: '[data-add-to-cart-text]',
  comparePrice: '[data-compare-price]',
  comparePriceText: '[data-compare-text]',
  originalSelectorId: '[data-product-select]',
  priceWrapper: '[data-price-wrapper]',
  productImageWrapper: '[data-product-image-wrapper]',
  productFeaturedImage: '[data-product-featured-image]',
  productJson: '[data-product-json]',
  productPrice: '[data-product-price]',
  productThumbs: '[data-product-single-thumbnail]',
  singleOptionSelector: '[data-single-option-selector]',
};

const cssClasses = {
  activeThumbnail: 'active-thumbnail',
  hide: 'hide',
};

/**
 * Product section constructor. Runs on page load as well as Theme Editor
 * `section:load` events.
 * @param {string} container - selector for the section container DOM element
 */

sections.register('product', {
  onLoad() {

    // Stop parsing if we don't have the product json script tag when loading
    // section in the Theme Editor
    if (!$(selectors.productJson, this.$container).html()) {
      return;
    }

    this.productSingleObject = JSON.parse(
      $(selectors.productJson, this.$container).html(),
    );

    const options = {
      $container: this.$container,
      enableHistoryState: this.$container.data('enable-history-state') || false,
      singleOptionSelector: selectors.singleOptionSelector,
      originalSelectorId: selectors.originalSelectorId,
      product: this.productSingleObject,
    };

    this.settings = {};
    this.variants = new Variants(options);
    this.$featuredImage = $(selectors.productFeaturedImage, this.$container);

    this.$container.on(
      `variantChange${this.namespace}`,
      this.updateAddToCartState.bind(this),
    );
    this.$container.on(
      `variantPriceChange${this.namespace}`,
      this.updateProductPrices.bind(this),
    );

    if (this.$featuredImage.length > 0) {
      this.$container.on(
        `variantImageChange${this.namespace}`,
        this.updateImages.bind(this),
      );
    }

    // HB modified code

    // Product quiz calculations

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    function getUrlParam(parameter, defaultvalue = ''){
        var urlparameter = defaultvalue;
        if(window.location.href.indexOf(parameter) > -1){
            urlparameter = getUrlVars()[parameter];
        }
        return urlparameter;
    }

    var currentWeight = getUrlParam( 'cw' );
    var adultWeight = decodeURI( getUrlParam( 'aw' ) );
    var bodyShape = decodeURI( getUrlParam( 'bs' ) );
    var activityLevel = decodeURI( getUrlParam( 'al' ) );
    var dateOfBirth = new Date( decodeURI( getUrlParam( 'age' ) ) );
    var now = new Date();
    var age = ( now - dateOfBirth ) / 1000 / 60 / 60 / 24 / 365;

    var lbToKgConstant = 0.45359237;

    if ( currentWeight && adultWeight && bodyShape && activityLevel ) {

      console.log( 'Age: ', age );

      var isPuppy = false;

      if ( ( adultWeight >= 50 && age < 2 ) || ( adultWeight < 50 && age < 1.5 )  )
        isPuppy = true;

      console.log( 'Is puppy? ', isPuppy );

      var currentWeightKg = currentWeight * lbToKgConstant;
      var adultWeightKg = adultWeight * lbToKgConstant;

      // start score with current weight in kg
      var score = currentWeightKg;

      var bodyMultiplier = 1;
      switch ( bodyShape ) {
        case 'Thin' :
          bodyMultiplier += 0.4;
          break;
        case 'Thinnish' :
          bodyMultiplier += 0.2;
          break;
        case 'Medium' :
          bodyMultiplier += 0;
          break;
        case 'Heavier' :
          bodyMultiplier += -0.2;
          break;
        case 'Obese' :
          bodyMultiplier += -0.4;
          break;
      } // switch

      // find ideal weight by multiplying current weight by "Body Conditioning Score"
      score = score * bodyMultiplier;

      var activityMultiplier = 100;
      switch ( bodyShape ) {
        case 'Very Active' :
          bodyMultiplier += 25;
          break;
        case 'Moderate' :
          bodyMultiplier += 25;
          break;
        case 'Inactive' :
          bodyMultiplier += -5;
          break;
      } // switch

      // =130*(D5^0.75)*3.2*(EXP(-0.87*(D5/C22)-0.1))

      score = Math.pow(score, 0.75) * activityMultiplier;

      // round to get daily caloric needs

      score = Math.ceil( score );

      if (score <= 402) {
        console.log( '1 bag' );
        $( '.product-option-select' ).val( $( '.product-option-select  option:nth-child(1)' ).val() ).trigger( 'change' );
      } else if (score <= 803) {
        console.log( '2 bags' );
        $( '.product-option-select' ).val( $( '.product-option-select  option:nth-child(2)' ).val() ).trigger( 'change' );
      } else if (score <= 1205) {
        console.log( '3 bags' );
        $( '.product-option-select' ).val( $( '.product-option-select  option:nth-child(3)' ).val() ).trigger( 'change' );
      } else if (score <= 1606) {
        console.log( '4 bags' );
        $( '.product-option-select' ).val( $( '.product-option-select  option:nth-child(4)' ).val() ).trigger( 'change' );
      } else if (score <= 2008) {
        console.log( '5 bags' );
        $( '.product-option-select' ).val( $( '.product-option-select  option:nth-child(5)' ).val() ).trigger( 'change' );
      } else if (score <= 2409) {
        console.log( '6 bags' );
        $( '.product-option-select' ).val( $( '.product-option-select  option:nth-child(6)' ).val() ).trigger( 'change' );
      } else if (score <= 2811) {
        console.log( '7 bags' );
        $( '.product-option-select' ).val( $( '.product-option-select  option:nth-child(7)' ).val() ).trigger( 'change' );
      } else {
        $( '.product-option-select' ).val( $( '.product-option-select  option:last-child' ).val() ).trigger( 'change' );
        console.log( 'More than 7 bags' );

        // original technique was to forward page to specific variant. Too specific with different products etc.
        // window.location.replace( '/products/salmon-apples-carrots-peas?variant=18469994430582' );
      }

      // $( '.product-option-select' ).change();

      console.log( 'Daily Caloric Needs: ', score, ', Current Weight: ', currentWeight, ', Adult Weight: ', adultWeight, ', Body Shape: ',  bodyShape, ', Activity Level: ',  activityLevel );

      var growthPhaseScore = 130 * Math.pow( currentWeightKg, .75 ) * 3.2 * ( Math.pow( Math.E, -0.87 * currentWeightKg / adultWeightKg ) - 0.1 );

      // var caloriesPerCup = 365; // getting this from theme settings

      var cupsPerDay = growthPhaseScore / caloriesPerCup;

      var cupsPerServing = Math.ceil( cupsPerDay / 2 );

      console.log( 'Growth Phase Score: ', Math.ceil( growthPhaseScore ), ', caloriesPerCup: ', caloriesPerCup, ', cupsPerDay: ', cupsPerDay, ', cupsPerServing: ', cupsPerServing );

      if ( cupsPerServing == 1 ) {
        var cupPlural = '';
      } else {
        var cupPlural = 's';
      }

      $( '.js-cups-per-serving' ).html( serving_rec_text_1 + ' ' + cupsPerServing + ' cup' + cupPlural + ' ' + serving_rec_text_2 );


    } // endif

    $( '.product-option-select' ).change();

    // End HB modified code

  },

  setActiveThumbnail(imageId) {
    let newImageId = imageId;

    // If "imageId" is not defined in the function parameter, find it by the current product image
    if (typeof newImageId === 'undefined') {
      newImageId = $(
        `${selectors.productImageWrapper}:not('.${cssClasses.hide}')`,
      ).data('image-id');
    }

    const $thumbnail = $(
      `${selectors.productThumbs}[data-thumbnail-id='${newImageId}']`,
    );

    $(selectors.productThumbs)
      .removeClass(cssClasses.activeThumbnail)
      .removeAttr('aria-current');

    $thumbnail.addClass(cssClasses.activeThumbnail);
    $thumbnail.attr('aria-current', true);
  },

  switchImage(imageId) {
    const $newImage = $(
      `${selectors.productImageWrapper}[data-image-id='${imageId}']`,
      this.$container,
    );
    const $otherImages = $(
      `${selectors.productImageWrapper}:not([data-image-id='${imageId}'])`,
      this.$container,
    );
    $newImage.removeClass(cssClasses.hide);
    $otherImages.addClass(cssClasses.hide);
  },

  /**
   * Updates the DOM state of the add to cart button
   *
   * @param {boolean} enabled - Decides whether cart is enabled or disabled
   * @param {string} text - Updates the text notification content of the cart
   */
  updateAddToCartState(evt) {
    const variant = evt.variant;

    if (variant) {
      $(selectors.priceWrapper, this.$container).removeClass(cssClasses.hide);
    } else {
      $(selectors.addToCart, this.$container).prop('disabled', true);
      $(selectors.addToCartText, this.$container).html(
        theme.strings.unavailable,
      );
      $(selectors.priceWrapper, this.$container).addClass(cssClasses.hide);
      return;
    }

    if (variant.available) {
      $(selectors.addToCart, this.$container).prop('disabled', false);
      $(selectors.addToCartText, this.$container).html(theme.strings.addToCart);
    } else {
      $(selectors.addToCart, this.$container).prop('disabled', true);
      $(selectors.addToCartText, this.$container).html(theme.strings.soldOut);
    }
  },

  updateImages(evt) {
    const variant = evt.variant;
    const imageId = variant.featured_image.id;

    this.switchImage(imageId);
    this.setActiveThumbnail(imageId);
  },

  /**
   * Updates the DOM with specified prices
   *
   * @param {string} productPrice - The current price of the product
   * @param {string} comparePrice - The original price of the product
   */
  updateProductPrices(evt) {
    const variant = evt.variant;
    const $comparePrice = $(selectors.comparePrice, this.$container);
    const $compareEls = $comparePrice.add(
      selectors.comparePriceText,
      this.$container,
    );

    $(selectors.productPrice, this.$container).html(
      formatMoney(variant.price, theme.moneyFormat),
    );

    // HB modified code
    $( '.js-weekly-price' ).html(
      formatMoney(variant.price / 4, theme.moneyFormat),
    );
    // End HB modified code

    if (variant.compare_at_price > variant.price) {
      $comparePrice.html(
        formatMoney(variant.compare_at_price, theme.moneyFormat),
      );
      $compareEls.removeClass(cssClasses.hide);
    } else {
      $comparePrice.html('');
      $compareEls.addClass(cssClasses.hide);
    }
  },

  /**
   * Event callback for Theme Editor `section:unload` event
   */
  onUnload() {
    this.$container.off(this.namespace);
  },
});