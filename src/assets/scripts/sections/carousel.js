import $ from 'jquery';
import slick from 'slick-carousel';

// $( '.js-carousel' ).slick({
// 	dots: true,
// 	arrows: false,
// 	autoplay: true,
// 	pauseOnFocus: false,
// 	pauseOnHover: false,
// 	autoplaySpeed: 5000, // 3000 default
// });

$( '.js-quote-carousel' ).each( function() {

    var $slickElement = $( this );
    var $currentSlideNumber = $slickElement.next( '.carousel-nav' ).find( '.carousel-slide-count' );

    $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $currentSlideNumber.text(i + '/' + slick.slideCount);
    });

	$slickElement.slick({
		dots: false,
		arrows: true,
		autoplay: false,
		infinite: false,
		speed: 1000,
		prevArrow: $( '.slick-prev' ),
		nextArrow: $( '.slick-next' ),
	});

});

// $( '.js-product-detail-carousel' ).slick({
// 	dots: true,
// 	arrows: false,
// 	autoplay: false
// });